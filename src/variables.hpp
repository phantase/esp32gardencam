#include <stdint.h>

// WiFi SSID
extern const char* ssid;
// WiFi Password
extern const char* password;
// MQTT server URL
extern const char* mqtt_server;

// Sensor realm
extern const char* sensorRealm;

// Pictures destination
extern const char *post_url;