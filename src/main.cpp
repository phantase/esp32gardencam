#include <Arduino.h>
#include <esp_http_client.h>
#include <esp_camera.h>
#include <WiFi.h>
#include <PubSubClient.h>

#include <soc/soc.h>          // Disable brownour problems
#include <soc/rtc_cntl_reg.h> // Disable brownour problems
#include <driver/rtc_io.h>

#include <variables.hpp>

/* #region WiFi and MQTT variables */
  // WiFi SSID (comes from <variables.hpp>)
  // const char* ssid     = "";
  // WiFi Password (comes from <variables.hpp>)
  // const char* password = "";
  // MQTT server URL (comes from <variables.hpp>)
  // const char* mqtt_server = "";

  WiFiClient espClient;
  PubSubClient client(espClient);

  // Sensor realm (comes from <variables.hpp>)
  // const char* sensorRealm = "";
  // ESP Mac address (as unsigned char*)
  unsigned char mac[6];
  // Esp Mac address (as string)
  String clientMac = "";
  // Esp Mac address (as char*)
  char sensorMac[18];

  // Serial message
  char serialMessage[100];
  // MQTT topic
  char topic[50];
  // MQTT payload
  char payload[100];
/* #endregion */

/* #region Pictures capture variables */
// Pictures destination (comes from <variables.hpp>)
  // const char *post_url = "";
  // Flash pin
  // #define FLASH_PIN         4
  // CAMERA_MODEL_AI_THINKER
  #define PWDN_GPIO_NUM     32
  #define RESET_GPIO_NUM    -1
  #define XCLK_GPIO_NUM      0
  #define SIOD_GPIO_NUM     26
  #define SIOC_GPIO_NUM     27
  #define Y9_GPIO_NUM       35
  #define Y8_GPIO_NUM       34
  #define Y7_GPIO_NUM       39
  #define Y6_GPIO_NUM       36
  #define Y5_GPIO_NUM       21
  #define Y4_GPIO_NUM       19
  #define Y3_GPIO_NUM       18
  #define Y2_GPIO_NUM        5
  #define VSYNC_GPIO_NUM    25
  #define HREF_GPIO_NUM     23
  #define PCLK_GPIO_NUM     22
/* #endregion */

/* #region Deepsleep variables */
  // Conversion factor for micro seconds to seconds
  #define uS_TO_S_FACTOR 1000000
  // Time ESP32 will go to sleep (in seconds)
  #define TIME_TO_SLEEP  30
  // [DeepSleep data] boot counter
  RTC_DATA_ATTR int bootCount = 0;
/* #endregion */

/* #region Other variables */
  #define LED_BUILTIN     33
/* #endregion */

// Convert a mac address in uint8_t* to a mac address in string
String macToStr(const uint8_t* mac, String separator) {
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += separator;
  }
  return result;
}

// Connect to the wifi
void connect_wifi() {
  Serial.println("Starting connecting WiFi.");
  delay(10);
  WiFi.begin(ssid, password);
  int action_try = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    action_try++;
    if(action_try>60) {
      ESP.restart();
    }
  }
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  WiFi.macAddress(mac);
  clientMac += macToStr(mac,":");
  Serial.println(clientMac);
  macToStr(mac,"-").toCharArray(sensorMac,18);
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
  switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
      Serial.println("HTTP_EVENT_ERROR");
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_ERROR");
        client.publish(topic, payload);
      /* #endregion */
      break;
    case HTTP_EVENT_ON_CONNECTED:
      Serial.println("HTTP_EVENT_ON_CONNECTED");
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_ON_CONNECTED");
        client.publish(topic, payload);
      /* #endregion */
      break;
    case HTTP_EVENT_HEADER_SENT:
      Serial.println("HTTP_EVENT_HEADER_SENT");
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_HEADER_SENT");
        client.publish(topic, payload);
      /* #endregion */
      break;
    case HTTP_EVENT_ON_HEADER:
      Serial.println();
      Serial.printf("HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        client.publish(topic, payload);
      /* #endregion */
      break;
    case HTTP_EVENT_ON_DATA:
      Serial.println();
      Serial.printf("HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        client.publish(topic, payload);
      /* #endregion */
      if (!esp_http_client_is_chunked_response(evt->client)) {
        // Write out data
        // printf("%.*s", evt->data_len, (char*)evt->data);
      }
      break;
    case HTTP_EVENT_ON_FINISH:
      Serial.println("");
      Serial.println("HTTP_EVENT_ON_FINISH");
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_ON_FINISH");
        client.publish(topic, payload);
      /* #endregion */
      break;
    case HTTP_EVENT_DISCONNECTED:
      Serial.println("HTTP_EVENT_DISCONNECTED");
      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/httpevent",sensorRealm,sensorMac);
        snprintf(payload, 100, "HTTP_EVENT_DISCONNECTED");
        client.publish(topic, payload);
      /* #endregion */
      break;
  }
  return ESP_OK;
}

// Take picture and send to MQTT topics it has been done
static esp_err_t takePicturesAndSendValues() {
  
  int action_try = 0;
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(sensorMac)) {
      Serial.println("connected");

      /* #region Send to MQTT number of boot */
        snprintf(topic, 50, "sensors/%s/%s/message",sensorRealm,sensorMac);
        snprintf(payload, 100, "Boot#%d",bootCount);
        client.publish(topic, payload);
      /* #endregion */

      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
        snprintf(payload, 100, "BEFORE_TAKE_PICTURE");
        client.publish(topic, payload);
      /* #endregion */

      // Start flash before picture
      // digitalWrite(FLASH_PIN, HIGH);

      camera_fb_t * fb = NULL;
      esp_err_t res = ESP_OK;

      fb = esp_camera_fb_get();
      if (!fb) {
        Serial.println("Camera capture failed");
        /* #region Send to MQTT */
          snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
          snprintf(payload, 100, "FAIL_TAKE_PICTURE");
          client.publish(topic, payload);
          // Stop flash after picture, also if it has failed
          // digitalWrite(FLASH_PIN, LOW);
        /* #endregion */
        return ESP_FAIL;
      }

      // Stop flash after picture
      // digitalWrite(FLASH_PIN, LOW);

      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
        snprintf(payload, 100, "AFTER_TAKE_PICTURE");
        client.publish(topic, payload);
      /* #endregion */

      // Start internal Led to see that we are in the transmitting part
      digitalWrite(LED_BUILTIN, HIGH);

      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
        snprintf(payload, 100, "BEFORE_HTTP");
        client.publish(topic, payload);
      /* #endregion */

      esp_http_client_handle_t http_client;
      
      esp_http_client_config_t config_client = {0};
      config_client.url = post_url;
      config_client.event_handler = _http_event_handler;
      config_client.method = HTTP_METHOD_POST;
      config_client.timeout_ms = 30000;

      http_client = esp_http_client_init(&config_client);

      esp_http_client_set_post_field(http_client, (const char *)fb->buf, fb->len);

      esp_http_client_set_header(http_client, "Content-Type", "image/jpg");

      esp_err_t err = esp_http_client_perform(http_client);
      if (err == ESP_OK) {
        Serial.print("esp_http_client_get_status_code: ");
        Serial.println(esp_http_client_get_status_code(http_client));
        /* #region Send to MQTT */
          snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
          snprintf(payload, 100, "OK_HTTP");
          client.publish(topic, payload);
        /* #endregion */
      } else {
        /* #region Send to MQTT */
          snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
          snprintf(payload, 100, "FAIL_HTTP");
          client.publish(topic, payload);
        /* #endregion */
      }

      esp_http_client_cleanup(http_client);

      /* #region Send to MQTT */
        snprintf(topic, 50, "sensors/%s/%s/step",sensorRealm,sensorMac);
        snprintf(payload, 100, "AFTER_HTTP");
        client.publish(topic, payload);
      /* #endregion */

      // Stop internal Led after the transmitting part is over
      digitalWrite(LED_BUILTIN, LOW);

      esp_camera_fb_return(fb);

    } else {
      action_try++;
      // We let 10 try, if it doesn't work, abandon this send and go deep sleep...
      if(action_try>10){
        break;
      }
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }

  }
  
}

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector

  Serial.begin(115200);
  // Might take some time to open up the Serial Monitor
  delay(1000);

  Serial.println();

  // Increase the number of boot and write it to serial
  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  // Connect to WiFi
  connect_wifi();

  // Initialize MQTT
  Serial.println("Initializing MQTT");
  client.setServer(mqtt_server, 1883);

  // Initialize Camera
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  //init with high specs to pre-allocate larger buffers
  if (psramFound()) {
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }
  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    ESP.restart();
    return;
  }

  // rtc_gpio_hold_dis(GPIO_NUM_4);
  // pinMode(FLASH_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  // Take pictures and send alert to MQTT
  takePicturesAndSendValues();

  // rtc_gpio_hold_en(GPIO_NUM_4);

  // Enable a timer wakeup for deep sleep
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);

  Serial.println("Going to sleep now");
  Serial.flush(); 

  // 3 seconds delay to be sure all messages are flushed (including MQTT)
  delay(3000);
  
  // Go to deep sleep
  esp_deep_sleep_start();
}

void loop() {
  delay(5000);
}