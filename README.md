# Esp32GardenCam

This project uses an ESP32Cam (based on [ESP32-CAM AI-Thinker](https://randomnerdtutorials.com/esp32-cam-ai-thinker-pinout/)) to monitor garden (plant growing).

## Before you start

What you need for this project :

* Hardware
  * an ESP32-CAM (ex: ESP32-CAM AI-Thinker)
  * a FTDI to program it (or any other solution, be creative)
* Software
  * PlatformIO
  * a MQTT server (because the ESP32 will send all the event to it)
  * somewhere to POST pictures... I recommand a NodeRed somewhere (easy and ... easy to deploy and use)

## Configuration

Use the `variables.cpp.sample` file to create your own `variables.cpp` with your parameters inside.

## Build

Use the _build_ function of PlatformIO (the hardest part)

## And a gift, a flow for NodeRed to POST your pictures

```json
[
    {
        "id": "4e4f8992.21bf78",
        "type": "httpInMultipart",
        "z": "6b22e28e.16da9c",
        "name": "",
        "url": "/post-img",
        "method": "post",
        "fields": "[{\"name\":\"name\"},{\"name\":\"image\",\"maxCount\":1}]",
        "swaggerDoc": "",
        "x": 130,
        "y": 600,
        "wires": [
            [
                "1de7408e.e99a5f",
                "b894ed9c.389f2"
            ]
        ]
    },
    {
        "id": "1de7408e.e99a5f",
        "type": "debug",
        "z": "6b22e28e.16da9c",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "true",
        "targetType": "full",
        "x": 390,
        "y": 520,
        "wires": []
    },
    {
        "id": "93a9e5fc.e8ce98",
        "type": "template",
        "z": "6b22e28e.16da9c",
        "name": "",
        "field": "payload",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "<html>\n    <head></head>\n    <body>\n        <h1>Upload successful</h1>\n    </body>\n</html>",
        "output": "str",
        "x": 700,
        "y": 600,
        "wires": [
            [
                "1c38d7a5.f6bbb8"
            ]
        ]
    },
    {
        "id": "1c38d7a5.f6bbb8",
        "type": "http response",
        "z": "6b22e28e.16da9c",
        "name": "",
        "statusCode": "",
        "headers": {},
        "x": 930,
        "y": 600,
        "wires": []
    },
    {
        "id": "b894ed9c.389f2",
        "type": "change",
        "z": "6b22e28e.16da9c",
        "name": "",
        "rules": [
            {
                "t": "set",
                "p": "payload",
                "pt": "msg",
                "to": "req.body",
                "tot": "msg"
            },
            {
                "t": "set",
                "p": "filename",
                "pt": "msg",
                "to": "'/data/picupload/' & $millis() & '.jpg'\t",
                "tot": "jsonata"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 420,
        "y": 600,
        "wires": [
            [
                "93a9e5fc.e8ce98",
                "d2b0c0f5.eb90c"
            ]
        ]
    },
    {
        "id": "d2b0c0f5.eb90c",
        "type": "file",
        "z": "6b22e28e.16da9c",
        "name": "",
        "filename": "",
        "appendNewline": true,
        "createDir": false,
        "overwriteFile": "false",
        "encoding": "none",
        "x": 690,
        "y": 680,
        "wires": [
            []
        ]
    }
]
```
